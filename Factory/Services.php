<?php
/**
 * Services factory class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Services factory class
 *
 * @package Factory
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Factory_Services
{
    /**
     * DataMapperFactory instance
     *
     * @type Factory_DataMappers $_dataMapperFactory
     */
    protected $_dataMapperFactory;

    /**
     * DomainObjectFactory instance
     *
     * @type Factory_DomainObjects $_objectFactory
     */
    protected $_objectFactory;

    /**
     * Factory cache
     *
     * @type array $_cache
     */
    protected $_cache = array();

    /**
     * Create instance
     *
     * @param Factory_DataMappers   $dataMapperFactory
     * @param Factory_DomainObjects $objectFactory
     */
    public function __construct(Factory_DataMappers $dataMapperFactory, Factory_DomainObjects $objectFactory)
    {
        $this->_dataMapperFactory = $dataMapperFactory;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * Build recognition service instance
     *
     * @return Model_RecognitionService
     */
    public function buildRecognitionService()
    {
        if ($this->isCached('RecognitionService')) {
            return $this->_cache['RecognitionService'];
        }
        $instance = new Model_RecognitionService($this->_dataMapperFactory, $this->_objectFactory);
        $this->register('RecognitionService', $instance);
        return $instance;
    }

    /**
     * Build remoteapi service instance
     *
     * @return Model_RemoteapiService
     */
    public function buildRemoteapiService()
    {
        if ($this->isCached('RemoteapiService')) {
            return $this->_cache['RemoteapiService'];
        }
        $recognitionService = $this->buildRecognitionService();
        $instance = new Model_RemoteapiService(
            $this->_dataMapperFactory,
            $this->_objectFactory,
            $recognitionService,
            new HttpRequest());
        $this->register('RemoteapiService', $instance);
        return $instance;
    }

    /**
     * Register service
     *
     * @param string $name
     * @param object $instance
     *
     * @throws Exception if $instance is not an object
     */
    public function register($name, $instance)
    {
        if (!is_object($instance)) {
            throw new Exception("$instance is not an object");
        }
        if (!$this->isCached($name)) {
            $this->_cache[$name] = $instance;
        }
    }

    /**
     * Retrieve cached service instance
     *
     * @param $name
     *
     * @return object|null
     */
    public function retrieveCached($name)
    {
        if ($this->isCached($name)) {
            return $this->_cache[$name];
        }
        return null;
    }

    /**
     * Check if specified service object is cached
     *
     * @param string $name
     *
     * @return bool
     */
    protected function isCached($name)
    {
        return isset($this->_cache[$name]);
    }
}
