<?php
/**
 * Maps transaction related data
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Maps transaction related data
 *
 * @package Model
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Model_TransactionMapper extends Core_Mapper
{
    /**
     * Save transaction
     *
     * @param Model_TransactionObject $transaction Transaction domain object
     *
     * @return string
     * @throws PDOException if there was any error while inserting to the database
     */
    public function save(Model_TransactionObject $transaction)
    {
        $stmt = $this->_DB->prepare("INSERT INTO Transactions (user_id, error_code, message) VALUES (:userId, :errorCode, :message)");
        $data = array(
            ':userId' => $transaction->getUserId(),
            ':errorCode' => $transaction->getErrorCode(),
            ':message' => $transaction->getMessage()
        );
        $success = $stmt->execute($data);
        if ($success === false) {
            throw new PDOException('Error when inserting transaction to the database');
        }
        return $this->_DB->lastInsertId();
    }
}
