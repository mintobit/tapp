<?php
/**
 * Transaction domain object
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Transaction domain object
 *
 * @package Model
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Model_TransactionObject extends Core_Object
{
    /**
     * Transaction id
     * @type int $_id
     */
    protected $_id;

    /**
     * User id
     * @type int $_userId
     */
    protected $_userId;

    /**
     * Error code (0 || 1)
     * @type int $_errorCode
     */
    protected $_errorCode;

    /**
     * Transaction message
     * @type string $_message
     */
    protected $_message;

    /**
     * Datetime
     * @type string $_dt
     */
    protected $_dt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return (int)$this->_id;
    }

    /**
     * Set id
     *
     * @param string|int $id
     */
    public function setId($id)
    {
        $this->_id = (int)$id;
    }

    /**
     * Get user id
     *
     * @return int
     */
    public function getUserId()
    {
        return (int)$this->_userId;
    }

    /**
     * Set user id
     *
     * @param string|int $id
     */
    public function setUserId($id)
    {
        $this->_userId = (int)$id;
    }

    /**
     * Get datetime
     *
     * @return string
     */
    public function getDt()
    {
        return (string)$this->_dt;
    }

    /**
     * Set datetime
     *
     * @param string $dt
     */
    public function setDt($dt)
    {
        $this->_dt = (string)$dt;
    }

    /**
     * Get error code
     *
     * @return int
     */
    public function getErrorCode()
    {
        return (int)$this->_errorCode;
    }

    /**
     * Set error code
     *
     * @param string|int $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->_errorCode = (int)$errorCode;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return (string)$this->_message;
    }

    /**
     * Set message
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->_message = (string)$message;
    }

    /**
     * Validate transaction object
     *
     * @return array
     */
    public function validate()
    {
        $errors = array();
        if (!is_numeric($this->_userId)) {
            $errors['userId'] = 'UserId should be numeric';
        }
        if ($this->_errorCode > 1) {
            $errors['errorCode'] = 'ErrorCode should be either 0 or 1';
        }
        if (empty($this->_message)) {
            $errors['message'] = 'Message should not be empty';
        }
        return $errors;
    }
}
