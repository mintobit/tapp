<?php
/**
 * Abstract view class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Abstract view class
 *
 * @package Core
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
abstract class Core_View
{
    /**
     * ServiceFactory object
     * @type Factory_Services $_serviceFactory
     */
    protected $_serviceFactory;

    /**
     * Absolute path to the template directory
     * @type    string $_templatePath
     * @example /var/www/htdocs/templates/form/
     */
    protected $_templatePath;

    /**
     * Page source to be sent to the browser
     * @type string $_output
     */
    protected $_output;

    /**
     * Headers to be sent to the browser
     * @type array $_headers
     */
    protected $_headers = array();

    /**
     * If true, 404 page will be rendered
     * @type bool $_notFound
     */
    protected $_notFound;

    /**
     * Valid page Content-Types
     * @type array $_contentTypes
     */
    protected $_contentTypes
        = array('html' => 'Content-Type: text/html; charset=utf-8',
                'json' => 'Content-Type: application/json; charset=utf-8');

    /**
     * Create instance
     *
     * @param Factory_Services $serviceFactory
     */
    public function __construct(Factory_Services $serviceFactory)
    {
        $this->_serviceFactory = $serviceFactory;
        $this->_notFound = false;
    }

    /**
     * Set NotFound flag
     *
     * @param bool $flag
     */
    public function setNotFound($flag)
    {
        $this->_notFound = (bool)$flag;
    }

    /**
     * Set template directory path
     *
     * @param string $path Absolute path
     *
     * @throws Exception if $path is not readable
     */
    public function setTemplatePath($path)
    {
        if (!is_readable($path)) {
            throw new Exception("Invalid template path $path");
        }
        $this->_templatePath = $path;
    }

    /**
     * Set Content-Type header
     *
     * @param string $type Content-type declared at $_contentTypes
     *
     * @throws Exception If provided Content-Type is invalid
     */
    public function setContentType($type)
    {
        if (!isset($this->_contentTypes[$type])) {
            throw new Exception("Error when setting invalid Content-Type");
        }
        $this->_headers[] = $this->_contentTypes[$type];
    }

    /**
     * Render the output
     *
     * @throws Exception if output is empty
     */
    public function render()
    {
        if ($this->_notFound === true) {
            $this->addHeader('HTTP/1.0 404 Not Found');
            $this->_output = $this->fetch('404');
        }
        foreach ($this->_headers as $value) {
            header($value);
        }
        if (is_null($this->_output)) {
            throw new Exception("Output is empty");
        }
        echo $this->_output;
    }

    /**
     * Escape string
     *
     * @param string $string
     *
     * @return string
     */
    protected function escape($string)
    {
        return htmlspecialchars($string, 'ENT_QUOTES', 'UTF-8');
    }

    /**
     * Fetch the template and fill it with data
     *
     * @param string     $template
     * @param array|null $data
     *
     * @return string
     * @throws Exception if specified template was not found
     */
    protected function fetch($template, $data = null)
    {
        if (!$this->isTemplate($template)) {
            throw new Exception("Template file $this->_templatePath$template not found");
        }
        ob_start();
        if (is_array($data)) {
            extract($data, EXTR_SKIP);
        }
        require $this->_templatePath . $template . EXT;
        return ob_get_clean();
    }

    /**
     * Check if the specified template exists
     *
     * @param string $template
     *
     * @return bool
     */
    protected function isTemplate($template)
    {
        return is_readable($this->_templatePath . $template . EXT);
    }

    /**
     * Add header
     *
     * @param string $header
     */
    protected function addHeader($header)
    {
        array_push($this->_headers, $header);
    }

    /**
     * Append string to the output
     *
     * @param string $string
     */
    protected function appendOutput($string)
    {
        $this->_output = $this->_output . $string;
    }
}
