<?php
/**
 * Handles remote api communication
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Handles remote api communication
 *
 * @package Model
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Model_RemoteapiService extends Core_Service
{
    /**
     * RecognitionService object
     * @type Model_RecognitionService $_recognitionService
     */
    protected $_recognitionService;

    /**
     * HttpRequest object
     * @type HttpRequest $_httpRequest
     */
    protected $_httpRequest;

    /**
     * RemoteapiService messages
     * @type array $_messages
     */
    protected $_messages = array();

    /**
     * Create instance
     *
     * @param Factory_DataMappers   $dataMapperFactory
     * @param Factory_DomainObjects $objectFactory
     * @param Model_RecognitionService  $recognitionService
     * @param HttpRequest           $httpRequest
     */
    public function __construct(
        Factory_DataMappers $dataMapperFactory, Factory_DomainObjects $objectFactory,
        Model_RecognitionService $recognitionService, HttpRequest $httpRequest
    ) {
        parent::__construct($dataMapperFactory, $objectFactory);
        $this->_recognitionService = $recognitionService;
        $this->_httpRequest = $httpRequest;
    }

    /**
     * Send "contact me" request to the remote api
     *
     * @param string $email
     * @param string $phone
     * @param string $ip
     *
     * @throws Exception if transaction object is invalid
     */
    public function requestContact($email, $phone, $ip)
    {
        $user = $this->_objectFactory->buildUserObject();
        $transaction = $this->_objectFactory->buildTransactionObject();
        $userMapper = $this->_dataMapperFactory->buildUserMapper();
        $transactionMapper = $this->_dataMapperFactory->buildTransactionMapper();

        $userOptions = compact('email', 'phone', 'ip');
        $user->setOptions($userOptions);
        $this->appendErrors($user->validate());
        if (!empty($this->_errors)) {
            return;
        }

        array_pop($userOptions);
        $remoteMessage = $this->sendRequest('POST', $userOptions, 'http://test.stairwaysoft.net');
        $this->appendMessages($remoteMessage);
        if (!$this->_recognitionService->userExists($user)) {
            $user->setId($userMapper->save($user));
        }
        $usersFound = $userMapper->findByEmail($email);
        $user->setId($usersFound['0']['id']);

        $transactionOptions = array(
            'userId'    => $user->getId(),
            'errorCode' => $remoteMessage['code'] . "b",
            'message'   => $remoteMessage['message']
        );
        $transaction->setOptions($transactionOptions);
        $this->appendErrors($transaction->validate());
        if (!empty($this->_errors)) {
            throw new Exception("Error when validating transaction");
        }
        $transactionMapper->save($transaction);
    }

    /**
     * Get messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->_messages;
    }


    /**
     * Send http request
     *
     * @param string $method POST/GET
     * @param array  $fields
     * @param string $url
     *
     * @return mixed|string Response body
     * @throws Exception if (request method is invalid || remote api status code !== 200)
     */
    protected function sendRequest($method, array $fields, $url)
    {
        switch (strtoupper($method)) {
            case 'GET':
                $this->_httpRequest->setMethod(HTTP_METH_GET);
                $this->_httpRequest->addQueryData($fields);
                break;
            case 'POST':
                $this->_httpRequest->setMethod(HTTP_METH_POST);
                $this->_httpRequest->addPostFields($fields);
                break;
            default:
                throw new Exception('Error when sending request to remote API');
                break;
        }
        if (isset($url)) {
            $this->_httpRequest->setUrl($url);
        }
        $httpMessage = $this->_httpRequest->send();
        if ($httpMessage->getResponseCode() !== 200) {
            throw new Exception('Remote API returned' . $httpMessage->getResponseCode() . 'status code');
        }
        if ($httpMessage->getHeader('Content-type') === 'application/json') {
            return json_decode($httpMessage->getBody(), true);
        }
        return $httpMessage->getBody();

    }

    /**
     * Append messages
     *
     * @param array $messages
     */
    protected function appendMessages(array $messages)
    {
        $this->_messages = array_merge($this->_messages, $messages);
    }
}
