<?php
/**
 * Remote api communication controller
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Remote api communication controller
 *
 * @package Controller
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Controller_Remoteapi extends Core_Controller
{
    /**
     * Get email, phone and ip address, perform requestContact action on the remote api
     */
    public function requestContact()
    {
        if ($this->_request->getRequestedWith() === 'xmlhttprequest') {
            $ip = $this->_request->getIp();
            $email = $this->_request->getPost('email');
            $phone = $this->_request->getPost('phone');
            $remoteAPI = $this->_serviceFactory->buildRemoteapiService();
            $remoteAPI->requestContact($email, $phone, $ip);
        } else {
            $this->_view->setNotFound(true);
            $this->_view->setContentType('html');
        }
    }
}
