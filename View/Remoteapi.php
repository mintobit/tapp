<?php
/**
 * Remote api presentation class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Remote api presentation class
 *
 * @package View
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class View_RemoteAPI extends Core_View
{
    /**
     * Get errors and messages from remoteapi service, append json encoded array to output
     */
    public function requestContact()
    {
        $remoteApiService = $this->_serviceFactory->buildRemoteapiService();
        $data['errors'] = $remoteApiService->getErrors();
        $data['messages'] = $remoteApiService->getMessages();
        $this->appendOutput(json_encode($data));
    }
}
