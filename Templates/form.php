<script type="text/javascript">
    function contactMe() {
        email = $('#email').val();
        phone = $('#phone').val();
        $('div#notice').text('');
        $.post("<?php echo WEB_ROOT; ?>remoteapi/requestContact", { 'email':email, 'phone':phone },
                function (data) {
                    console.log(data.errors.length);
                    console.log(data.messages.length);
                    if (typeof(data.errors.length) == 'undefined') {
                        for (key in data.errors) {
                            $('div#notice').append('<p>' + data.errors[key] + '</p>');
                        }
                    } else if (typeof(data.messages.length) == 'undefined') {
                        $('div#notice').html(data.messages.message);
                    }
                }, "json");
    }
</script>
<div id="fieldset">
    <div class="field">
        <label class="field" for="email">Email </label>
        <input class="textbox" type="email" id="email" name="email" placeholder="example@domain.com">
    </div>

    <div class="field">
        <label class="field" for="phone">Phone </label>
        <input class="textbox" id="phone" type="tel" name="phone" placeholder="+380975554433">
    </div>

    <input class="textbox" value="Contact me" type="submit" onclick="javascript:contactMe();">
</div>
<div id="notice"></div>
