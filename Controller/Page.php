<?php
/**
 * Default page controller
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Default page controller
 *
 * @package Controller
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Controller_Page extends Core_Controller
{
    /**
     * No interaction here currently :)
     */
    public function index()
    {
    }
}
