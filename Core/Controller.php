<?php
/**
 * Abstract controller class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Abstract controller class
 *
 * @package Core
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
abstract class Core_Controller
{
    /**
     * ServiceFactory instance
     * @type Factory_Services $_serviceFactory
     */
    protected $_serviceFactory;

    /**
     * HTTP Request instance
     * @type Core_Request $_request
     */
    protected $_request;

    /**
     * View instance
     * @type Core_View $_view
     */
    protected $_view;

    /**
     * Create instance
     *
     * @param Factory_Services $serviceFactory
     * @param Core_Request     $request
     * @param Core_View        $view
     */
    public function __construct(Factory_Services $serviceFactory, Core_Request $request, Core_View $view)
    {
        $this->_serviceFactory = $serviceFactory;
        $this->_request = $request;
        $this->_view = $view;
    }
}
