<?php
/**
 * Handles initialization procedure
 *
 * @package tapp
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */


define('WEB_ROOT', 'http://web.dev/tapp/');
define('EXT', '.php');
define('PATH_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('PATH_TPL', realpath('Templates') . DIRECTORY_SEPARATOR);

define('DB_HOST', '192.168.56.2');
define('DB_NAME', 'tapp');
define('DB_USER', 'root');
define('DB_PASS', 'toor');

//set default timezone
date_default_timezone_set('Europe/Kiev');

error_reporting(-1);
ini_set('display_errors', 1);

set_exception_handler('exception_handler');
spl_autoload_register('autoload');

/**
 * Autoloader
 *
 * @param string $class Class name
 */
function autoload($class)
{
    $parts = explode('_', $class);
    $path = PATH_ROOT . implode(DIRECTORY_SEPARATOR, $parts);
    if (is_readable($path . EXT)) {
        require_once $path . EXT;
    }
}

/**
 * Exception handler
 *
 * @param $exception Exception instance
 */
function exception_handler($exception)
{
    echo $exception->getMessage();
}

//initialize DB connection
$DB = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=UTF8', DB_USER, DB_PASS,
    array(PDO::ATTR_ERRMODE          => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_EMULATE_PREPARES => false));
$DB->exec("SET time_zone = 'Europe/Kiev'");

//initialize ServiceFactory
$serviceFactory = new Factory_Services(new Factory_DataMappers($DB), new Factory_DomainObjects);

//initialize request
$request = new Core_Request();
//initialize router
$router = new Core_Router($request);
$router->run();
$serviceFactory->register('Router', $router);

//initialize View
$class = 'View_' . $router->getResourceName();
$command = $router->getCommand();
$view = new $class($serviceFactory);
$view->setTemplatePath(PATH_TPL);

//initialize Controller
$class = 'Controller_' . $router->getResourceName();
$controller = new $class($serviceFactory, $request, $view);
//execute command
$command = $router->getCommand();
$controller->{$command}();

//render View
$view->{$command}();
$view->render();