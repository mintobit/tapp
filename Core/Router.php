<?php
/**
 * Routes request to specific controller/action
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Routes request to specific controller/action
 *
 * @package Core
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Core_Router
{
    /**
     * HTTP Request instance
     * @type Core_Request
     */
    protected $_request;

    /**
     * Resource name (controller name in a broader sense)
     * @type string $_resourceName
     */
    protected $_resourceName;

    /**
     * Command (action name in a broader sense)
     * @type string $_command
     */
    protected $_command;

    /**
     * Shows if $this->run() was executed
     * @type bool $_running
     */
    protected $_running;

    /**
     * Create instance
     *
     * @param Core_Request $request
     */
    public function __construct(Core_Request $request)
    {
        $this->_request = $request;
        $this->_resourceName = 'Page';
        $this->_command = 'index';
        $this->_running = false;
    }

    /**
     * Get resource name
     *
     * @return string
     * @throws Exception if $this->run() was not executed
     */
    public function getResourceName()
    {
        if ($this->_running === true) {
            return $this->_resourceName;
        } else {
            throw new Exception('Run the router before getting the resource');
        }
    }

    /**
     * Set resource name
     *
     * @param string $resourceName
     */
    protected function setResourceName($resourceName)
    {
        $this->_resourceName = $resourceName;
    }

    /**
     * Get command
     *
     * @return string
     * @throws Exception if $this->run() was not executed
     */
    public function getCommand()
    {
        if ($this->_running === true)
        {
            return $this->_command;
        } else {
            throw new Exception('Run the router before getting the resource');
        }
    }

    /**
     * Set command
     *
     * @param string $command
     */
    protected function setCommand($command)
    {
        $this->_command = $command;
    }

    /**
     * Validate the route
     */
    protected function validate()
    {
        $this->_running = true;
        $class = 'Controller_' . $this->_resourceName;
        if (!class_exists($class)) {
            $this->setResourceName('404');
            $this->setCommand('index');
            return;
        }

        if (!in_array($this->_command, get_class_methods($class))) {
            $this->setResourceName('404');
            $this->_command = 'index';
        }
    }

    /**
     * Run router
     */
    public function run()
    {
        $resourceName = $this->_request->getSegment('controller');
        $command = $this->_request->getSegment('action');
        if (!empty($resourceName)) {
            $this->setResourceName(ucfirst(strtolower($resourceName)));
            if (!empty($command)) {
                $this->setCommand($command);
            }
        }
        $this->validate();
    }

}
