<?php
/**
 * Abstract mapper class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Abstract mapper class
 *
 * @package Core
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
abstract class Core_Mapper
{
    /**
     * PDO instance
     * @type PDO $_DB
     */
    protected $_DB;

    /**
     * Create instance
     *
     * @param PDO $DB
     */
    public function __construct(PDO $DB)
    {
        $this->_DB = $DB;
    }
}
