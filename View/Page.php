<?php
/**
 * Default page presentation class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Default page presentation class
 *
 * @package View
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class View_Page extends Core_View
{
    /**
     * Fetch head, form and footer templates, append them to the output
     */
    public function index()
    {
        $data['errors'] = array();
        $this->appendOutput($this->fetch('head'));
        $this->appendOutput($this->fetch('form', $data));
        $this->appendOutput($this->fetch('footer'));
    }
}
