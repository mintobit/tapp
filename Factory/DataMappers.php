<?php
/**
 * Data mappers factory class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Data mappers factory class
 *
 * @package Factory
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Factory_DataMappers
{
    /**
     * PDO instance
     * @type PDO $_DB
     */
    protected $_DB;

    /**
     * Factory cache
     * @type array $_cache
     */
    protected $_cache = array();

    /**
     * Create instance
     *
     * @param PDO $DB
     */
    public function __construct(PDO $DB)
    {
        $this->_DB = $DB;
    }

    /**
     * Build user mapper instance
     *
     * @return Model_UserMapper
     */
    public function buildUserMapper()
    {
        if ($this->isCached('UserMapper')) {
            return $this->_cache['UserMapper'];
        }
        $instance = new Model_UserMapper($this->_DB);
        $this->register('UserMapper', $instance);
        return $instance;
    }

    /**
     * Build transaction mapper instance
     *
     * @return Model_TransactionMapper
     */
    public function buildTransactionMapper()
    {
        if ($this->isCached('TransactionMapper')) {
            return $this->_cache['TransactionMapper'];
        }
        $instance = new Model_TransactionMapper($this->_DB);
        $this->register('TransactionMapper', $instance);
        return $instance;
    }

    /**
     * Register data mapper
     *
     * @param             $name
     * @param Core_Mapper $instance
     */
    public function register($name, Core_Mapper $instance)
    {
        if (!$this->isCached($name)) {
            $this->_cache[$name] = $instance;
        }
    }

    /**
     * Check if specified data mapper object is cached
     *
     * @param $name
     *
     * @return bool
     */
    protected function isCached($name)
    {
        return isset($this->_cache[$name]);
    }
}
