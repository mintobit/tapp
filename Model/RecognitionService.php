<?php
/**
 * Handles users related routines
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Handles users related routines
 *
 * @package Model
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Model_RecognitionService extends Core_Service
{
    /**
     * Check if user exists
     *
     * @param Model_UserObject $user
     *
     * @return bool
     */
    public function userExists(Model_UserObject $user)
    {
        $email = $user->getEmail();
        $userMapper = $this->_dataMapperFactory->buildUserMapper();
        $usersFoundByEmail = count($userMapper->findByEmail($email));
        if ($usersFoundByEmail > 0) {
            return true;
        }
        return false;
    }
}
