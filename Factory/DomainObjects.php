<?php
/**
 * Domain objects factory class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Domain objects factory class
 *
 * @package Factory
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Factory_DomainObjects
{
    /**
     * Build user domain object instance
     *
     * @return Model_UserObject
     */
    public function buildUserObject()
    {
        return new Model_UserObject();
    }

    /**
     * Build transaction domain object instance
     *
     * @return Model_TransactionObject
     */
    public function buildTransactionObject()
    {
        return new Model_TransactionObject();
    }
}
