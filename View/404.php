<?php
/**
 * 404 page presentation class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * 404 page presentation class
 *
 * @package View
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class View_404 extends Core_View
{
    /**
     * Fetch 404 template
     */
    public function index()
    {
        $this->appendOutput($this->fetch('404'));
    }
}
