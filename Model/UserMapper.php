<?php
/**
 * Maps user related data
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Maps user related data
 *
 * @package Model
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Model_UserMapper extends Core_Mapper
{
    /**
     * Find users by email
     *
     * @param string $email
     *
     * @return array
     * @throws PDOException if there is any error when performing database search
     */
    public function findByEmail($email)
    {
        $stmt = $this->_DB->prepare("SELECT id, INET_NTOA(ip) ip, email, phone FROM Users WHERE email = :email");
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $success = $stmt->execute();
        if ($success === false) {
            throw new PDOException("Error when searching user by phone number");
        }
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result === false) {
            return array();
        }
        return $result;
    }

    /**
     * Fin user by phone number
     *
     * @param string $phone
     *
     * @return array
     * @throws PDOException if there is any error when performing database search
     */
    public function findByPhone($phone)
    {
        $stmt = $this->_DB->prepare("SELECT * FROM Users WHERE phone = :phone");
        $stmt->bindValue(':phone', $phone, PDO::PARAM_STR);
        $success = $stmt->execute();
        if ($success === false) {
            throw new PDOException("Error when searching user by phone number");
        }
        return $stmt->fetchAll();
    }

    /**
     * Save user object
     *
     * @param Model_UserObject $user User domain object
     *
     * @return string
     * @throws PDOException
     */
    public function save(Model_UserObject $user)
    {
        $data = array(':ip'    => $user->getIp(),
                      ':email' => $user->getEmail(),
                      ':phone' => $user->getPhone());
        $stmt = $this->_DB->prepare("INSERT INTO Users (ip, email, phone) VALUES (INET_ATON(:ip),:email,:phone)");
        $success = $stmt->execute($data);
        if ($success === false) {
            throw new PDOException("Error when inserting user to the database");
        }
        return $this->_DB->lastInsertId();
    }
}
