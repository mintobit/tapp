<?php
/**
 * 404 page controller
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * 404 page controller
 *
 * @package Controller
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Controller_404 extends Core_Controller
{
    /**
     * Set NotFound flag in the view instance
     */
    public function index()
    {
    }
}
