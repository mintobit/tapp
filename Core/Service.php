<?php
/**
 * Abstract service class
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Abstract service class
 *
 * @package Core
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
abstract class Core_Service
{
    /**
     * DataMapperFactory instance
     * @type Factory_DataMappers $_dataMapperFactory
     */
    protected $_dataMapperFactory;

    /**
     * ObjectFactory object
     * @type Factory_DomainObject $_objectFactory
     */
    protected $_objectFactory;

    /**
     * Service errors
     * @type array $_errors
     */
    protected $_errors = array();

    /**
     * Create instance
     *
     * @param Factory_DataMappers   $dataMapperFactory
     * @param Factory_DomainObjects $objectFactory
     */
    public function __construct(Factory_DataMappers $dataMapperFactory, Factory_DomainObjects $objectFactory)
    {
        $this->_dataMapperFactory = $dataMapperFactory;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * Get errors
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * Append errors
     *
     * @param array $errors
     *
     */
    protected function appendErrors(array $errors)
    {
        $this->_errors = array_merge($this->_errors, $errors);
    }
}