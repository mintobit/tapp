<?php
/**
 * Handles http request
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * Handles http request
 *
 * @package Core
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Core_Request
{
    /**
     * Allowed request methods
     * @type array $_methods
     */
    protected $_methods
        = array('GET',
                'POST');

    /**
     * Request segments (controller, action, params)
     * @type array $_segments
     */
    protected $_segments = array();

    /**
     * Default request method
     * @type string $_defaultMethod
     */
    protected $_defaultMethod = 'GET';

    /**
     * Request method
     * @type string $_requestMethod
     */
    protected $_requestMethod;

    /**
     * Request url
     * @type string $_requestUrl
     */
    private $_requestUrl;

    /**
     * Parsed query string
     * @type array $_query
     */
    private $_query;

    /**
     * Create instance
     */
    public function __construct()
    {
        $this->_requestMethod = $this->detectMethod();
        $this->_requestUrl = $this->parseUrl();
    }

    /**
     * Get http_x_requested_with header
     *
     * @return string
     */
    public function getRequestedWith()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) : '';
    }

    /**
     * Get client ip address
     *
     * @return string
     */
    public function getIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Get post field value
     *
     * @param string $key
     *
     * @return string|null
     */
    public function getPost($key)
    {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }
        return null;
    }

    /**
     * Get query (get) field value
     *
     * @param $key
     *
     * @return string|null
     */
    public function getQuery($key)
    {
        if (isset($this->_query[$key])) {
            return $this->_query[$key];
        }
        return null;
    }

    /**
     * Detect request method
     *
     * @return string
     */
    public function detectMethod()
    {
        $method = $_SERVER['REQUEST_METHOD'];

        if ($this->validateMethod($method)) {
            return strtoupper($method);
        } else {
            return strtoupper($this->_defaultMethod);
        }
    }

    /**
     * Set request method
     *
     * @param $method
     */
    public function setMethod($method)
    {
        if ($this->validateMethod($method)) {
            $this->_requestMethod = $method;
        } else {
            $this->_requestMethod = $this->_defaultMethod;
        }
    }

    /**
     * Validate request method
     *
     * @param $method
     *
     * @return bool
     */
    public function validateMethod($method)
    {
        return (in_array($method, $this->_methods)) ? true : false;
    }

    /**
     * Get request method
     *
     * @return string
     */
    public function getMethod()
    {
        return strtoupper($this->_requestMethod);
    }

    /**
     * Parse request uri
     *
     * @param string|null $url
     *
     * @return string
     */
    public function parseUrl($url = null)
    {
        $url = (isset($url)) ? $url : $_SERVER['REQUEST_URI'];
        $queryString = parse_url($url, PHP_URL_QUERY);
        parse_str($queryString, $this->_query);
        $script = trim($_SERVER['SCRIPT_NAME'], "/");
        if (strpos($url, "?")) {
            list($url,) = explode("?", $url);
        }

        $url = trim($url, "/");
        $url = preg_replace('#/+#', '/', $url);

        $parts = array_diff(
            explode("/", $url),
            explode("/", $script)
        );
        $parts = array_values($parts);
        $this->setSegments($parts);

        $url = implode("/", $parts);
        return $url;
    }

    /**
     * Set request segments
     *
     * @param array $parts
     */
    public function setSegments(array $parts)
    {
        $parts = array_pad($parts, 2, "");
        $this->_segments['controller'] = array_shift($parts);
        $this->_segments['action'] = array_shift($parts);
        $this->_segments['params'] = $parts;
    }

    /**
     * Get segment
     *
     * @param $key
     *
     * @return string|null
     */
    public function getSegment($key)
    {
        if (isset($this->_segments[$key])) {
            return $this->_segments[$key];
        }
        return null;
    }

    /**
     * Set request url
     *
     * @param $url
     */
    public function setUrl($url)
    {
        $this->_requestUrl = $url;
    }

    /**
     * Get request url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->_requestUrl;
    }

}