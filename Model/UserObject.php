<?php
/**
 * User domain object
 *
 * @copyright Copyright (c) 2013 Anton Nizhegorodov
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version   1.0.0
 */

/**
 * User domain object
 *
 * @package Model
 * @author  Anton Nizhegorodov <anizh3gorodov@gmail.com>
 */
class Model_UserObject extends Core_Object
{
    /**
     * User id
     * @type int $_id
     */
    protected $_id;

    /**
     * User ip address
     * @type string $_ip
     */
    protected $_ip;

    /**
     * User email address
     * @type string $_email
     */
    protected $_email;

    /**
     * User phone number(international format)
     * @type string $_phone
     */
    protected $_phone;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return (int)$this->_id;
    }

    /**
     * Set id
     *
     * @param string|int $id
     */
    public function setId($id)
    {
        $this->_id = (int)$id;
    }

    /**
     * Get ip address
     *
     * @return string
     */
    public function getIp()
    {
        return (string)$this->_ip;
    }

    /**
     * Set ip address
     *
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->_ip = (string)$ip;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return (string)$this->_email;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->_email = (string)$email;
    }

    /**
     * Get phone number
     *
     * @return string
     */
    public function getPhone()
    {
        return (string)$this->_phone;
    }


    /**
     * Set phone number
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->_phone = (string)$phone;
    }

    /**
     * Validate user object
     *
     * @return array
     */
    public function validate()
    {
        $errors = array();
        if (!filter_var($this->_email, FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Please provide correct email address';
        }
        if (!preg_match('/^\+\d{7,15}$/', $this->_phone)) {
            $errors['phone'] = 'Please provide correct phone number';
        }
        return $errors;
    }
}
